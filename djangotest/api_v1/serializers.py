from rest_framework import serializers
from djangotest.campaigns.models import Product, Matching

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        pandas_index = ['id']

class MatchingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Matching
        pandas_index = ['id']

