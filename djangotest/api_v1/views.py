from rest_pandas import PandasView, PandasBaseRenderer, mkstemp, os
from djangotest.campaigns.models import Product, Matching
from djangotest.api_v1.serializers import ProductSerializer, MatchingSerializer

# Short version (leverages default DRP settings):
class ProductView(PandasView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class MatchingView(PandasView):
    queryset = Matching.objects.all()
    serializer_class = MatchingSerializer
