from django.test import TestCase
from djangotest.initdata.FactoryTestData import FactoryTestData
from djangotest.campaigns.models import *

class AnimalTestCase(TestCase):

    f = FactoryTestData()
    f.generate_campaign(12)
    f.generate_product(10)
    f.generate_competitor(9)
    f.generate_matching()

    def test_generate_campaign(self):

        list_camp = Campaign.objects.all()
        self.assertEqual(list_camp.count(), 12)
        self.assertEqual(list_camp[1].name, 'Campaign1')

    def test_generate_product(self):
        list_p = Product.objects.all()
        self.assertEqual(list_p.count(), 10)

    def test_generate_competitor(self):
        list_c = Competitor.objects.all()
        self.assertEqual(list_c.count(), 9)


