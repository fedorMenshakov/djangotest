
from optparse import make_option
import  os

from djangotest.initdata.FactoryTestData import FactoryTestData

os.environ['DJANGO_SETTINGS_MODULE'] = 'djangotest.settings'

from djangotest.campaigns.models import *

from django.core.management.base import BaseCommand





class Command(BaseCommand):
    help = 'Test data'
    option_list = BaseCommand.option_list + (
        make_option('--campaigns',
                    action='store',
                    dest='campaigns',
                    default=9,
                    help='Count campaigns'),
        make_option('--products',
                    action='store',
                    dest='products',
                    default=9,
                    help='Count products'),
        make_option('--competitors',
                    action='store',
                    dest='competitors',
                    default=2,
                    help='Count competitors'),
    )

    def handle(self, *args, **options):
        f = FactoryTestData()

        c = Campaign.objects.all()
        c.delete()
        p = Product.objects.all()
        p.delete()
        com = Competitor.objects.all()
        com.delete()
        m = Matching.objects.all()
        m.delete()

        f.generate_campaign(options['campaigns'])
        f.generate_product(options['products'])
        f.generate_competitor(options['competitors'])
        f.generate_matching()




c = Campaign.objects.all()
p = Product.objects.all()
com = Competitor.objects.all()
m = Matching.objects.all()

for i in c:
    print (i.id)
    print (i.name)
    print (i.domain)
    print (i.is_active)
print ('-----------')
for i in p:
    print (i.id)
    print (i.campaign)
    print (i.title)
    print (i.price)
print ('============')
for i in com:
    print (i)
print ('++++++++++++')
for i in m:
    print (i)
print ('*************')







