
from djangotest.campaigns.models import *
import random as r

class FactoryTestData:

    list_campaing = []
    list_product = []
    list_competitor = []
    list_matching = []


    def generate_campaign(self, campaigns):
        n = 0
        r.seed(campaigns)
        while n < campaigns:
            n += 1
            cam = Campaign.objects.create(name='Campaign' + str(n + 1),
                                          type=r.choice([0, 1]),
                                          domain='https://domain' + str(n + 1) + '.com',
                                          is_active=r.choice([True, False]),
                                          is_scanning_enabled=r.choice([True, False]),
                                          is_url_rewriting_enabled=r.choice([True, False]))
        self.list_campaing = Campaign.objects.all()

    def generate_product(self, products):
        r.seed(products)
        for c in self.list_campaing:
            n = 0
            while n < products:
                a = r.randint(1, 1001)
                b = r.uniform(1, 1000)
                n += 1
                pr = Product.objects.create(campaign=c,
                                        url='https://domain/product' + str(n + 1) + '.com',
                                        title='Title Product ' + str(n + 1),
                                        price=r.choice([a, b]))
        self.list_product = Product.objects.all()


    def generate_competitor(self, competitor):
        for cam in self.list_campaing:
            n = 0
            while n < competitor:
                n += 1
                com = Competitor.objects.create(campaign=cam,
                                                name='Competitor' + str(n + 1),
                                                domain='https://domain-competitor' + str(n + 1) + '.com')
        self.list_competitor = Competitor.objects.all()

    def generate_matching(self):
            r.seed(len(self.list_product) + len(self.list_campaing) + len(self.list_competitor))
            matching = 3
            n = 0
            while n < matching:
                m = Matching()
                a = len(self.list_matching)
                b = len(self.list_product)
                product = self.list_product[r.randint(1, len(self.list_product) - 1)]
                competitor = self.list_competitor[r.randint(1, len(self.list_competitor))]
                url = 'https://matching' + str(n + 1) + '.com'
                price = r.choice([r.randint(1, 1001), round(r.uniform(1, 1001), 2)])
                n += 1
                match = Matching.objects.create(product=product,
                                                competitor=competitor,
                                                url=url,
                                                price=price)
            self.list_matching=Matching.objects.all()