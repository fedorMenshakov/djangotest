from django.conf.urls import patterns, include, url
from djangotest.api_v1.views import ProductView
urlpatterns = patterns('',
    url(r'^product', ProductView.as_view()),
    url(r'^matching', ProductView.as_view()),
    )

# This is only required to support extension-style formats (e.g. /data.csv)
from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = format_suffix_patterns(urlpatterns)